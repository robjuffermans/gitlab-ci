#!/bin/sh
git clone --branch $CI_COMMIT_REF_NAME git@gitlab.com:maxmath/maxmath_api.git git_root
mv git_root/maxmath_api maxmath_api
rm -rf git_root

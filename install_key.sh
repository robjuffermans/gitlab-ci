#!/bin/bash
yum install -y --quiet epel-release
yum install -y --quiet which
which ssh-agent || ( yum install -y --quiet openssh-clients )
eval $(ssh-agent -s)
ssh-add <(echo "$SSH_PRIVATE_KEY")
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

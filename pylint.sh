# uv add pylint
# find ${1:-.} -iname "*.py" | xargs uv run pylint --extension-pkg-whitelist='pydantic' -E --disable=E0401,E0611
# uv add pylint
# uv add pytest
uv sync --frozen
uv run pylint -E ${1:-.}
# uvx pylint -E ${1:-.}

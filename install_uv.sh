#!/bin/sh
{
  apk update && apk upgrade && apk add curl
} || command_failed=1

if [ ${command_failed:-0} -eq 1 ]
then
 echo "No Alpine linux"
fi
curl -LsSf https://astral.sh/uv/install.sh | sh
source $HOME/.local/bin/env

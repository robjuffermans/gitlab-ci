#!/bin/sh
git clone --branch $CI_COMMIT_REF_NAME git@gitlab.com:maxmath/maxmath_async_lib.git git_root
mv git_root/maxmath_async maxmath_async
rm -rf git_root
